package hr.rateme.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="promotions")
public class Promotion implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3470561165862023354L;

	@Id
	@GeneratedValue
	@Column(name="id")
	@JsonProperty
	private int id;
	
	@Column(name="name")
	@JsonProperty
	private String name;
	
	@Column(name="description")
	@JsonProperty
	private String description;
	
	public Promotion() {
		super();
	}

	public Promotion(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}
	
	public Promotion(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion other = (Promotion) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Promotion [name=" + name + ", description=" + description + "]";
	}
	
	
	
}
