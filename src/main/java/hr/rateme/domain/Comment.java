package hr.rateme.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="comments")
public class Comment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3080577604850063653L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	@JsonProperty
	private int id;
	
	@Column(name="device_id")
	@JsonProperty
	private String device;
	
	@Column(name="text")
	@JsonProperty
	private String text;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="CET")
	@Column(name="date")
	@JsonProperty
	private Date date;

	public Comment(){
		super();
	}
	
	public Comment(String device, String text, Date date) {
		super();
		this.device = device;
		this.text = text;
		this.date = date;
	}
	
	public Comment(int id, String device, String text, Date date) {
		super();
		this.id=id;
		this.device = device;
		this.text = text;
		this.date = date;
	}
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getDevice() {
		return device;
	}



	public void setDevice(String device) {
		this.device = device;
	}



	public String getText() {
		return text;
	}



	public void setText(String text) {
		this.text = text;
	}


	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "Comment [device=" + device + ", text=" + text + ", date=" + date
				+ "]";
	}

	
	
}
