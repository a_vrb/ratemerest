package hr.rateme.domain;

import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="places")
public class Place {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	@JsonProperty
	private int id;

	@Column(name="name")
	@JsonProperty
	private String name;

	@Column(name="address")
	@JsonProperty
	private String address;
	
	@Column(name="picture_url")
	@JsonProperty
	private String pictureUrl;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="place_id")
	@JsonProperty
	private Set<Rating> ratings;

	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="place_id")
	@JsonProperty
	private Set<Comment> comments;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="place_id")
	@JsonProperty
	private Set<Promotion> promotions;

	@ElementCollection (fetch=FetchType.EAGER)
	@CollectionTable(name="beverages" , joinColumns=@JoinColumn(name="place_id"))
	@MapKeyColumn(name="type")
	@MapKeyEnumerated(EnumType.STRING)
	@Column(name="price")
	@JsonProperty
	private Map<BeverageType, Float> beverages;
	
	@Column(name="working_hours_from")
	@JsonProperty
	private String working_hours_from;

	@Column(name="working_hours_to")
	@JsonProperty
	private String working_hours_to;
	
	@Column(name="wifi")
	@JsonProperty
	private Boolean wifi;
	
	@Column(name="coffee_distributor")
	@JsonProperty
	private String coffee_distributor;
	
	@Column(name="smoking")
	@Enumerated(EnumType.STRING)
	@JsonProperty
	private Smoking smoking;
	
	public Place() {

	}
	
	public Place(int id, String name, String address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}

	public Place(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(Set<Promotion> promotions) {
		this.promotions = promotions;
	}

	public Map<BeverageType, Float> getBeverages() {
		return beverages;
	}

	public void setBeverages(Map<BeverageType, Float> beverages) {
		this.beverages = beverages;
	}
	
	public String getWorking_hours_from() {
		return working_hours_from;
	}

	public void setWorking_hours_from(String working_hours_from) {
		this.working_hours_from = working_hours_from;
	}

	public String getWorking_hours_to() {
		return working_hours_to;
	}

	public void setWorking_hours_to(String working_hours_to) {
		this.working_hours_to = working_hours_to;
	}

	public Boolean getWifi() {
		return wifi;
	}

	public void setWifi(Boolean wifi) {
		this.wifi = wifi;
	}

	public String getCoffee_distributor() {
		return coffee_distributor;
	}

	public void setCoffee_distributor(String coffee_distributor) {
		this.coffee_distributor = coffee_distributor;
	}

	public Smoking getSmoking() {
		return smoking;
	}

	public void setSmoking(Smoking smoking) {
		this.smoking = smoking;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Place other = (Place) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Place2 [id=" + id + ", name=" + name + ", address=" + address
				+ "]";
	}



}
