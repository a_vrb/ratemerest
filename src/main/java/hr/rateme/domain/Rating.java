package hr.rateme.domain;

import java.util.Date;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="ratings")
public class Rating implements Comparable<Rating>{
	
	@Id
	@GeneratedValue
	@Column(name="id")
	@JsonProperty
	private int id;
	
	@Column(name="devices_id")
	@JsonProperty
	private String device;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="CET")
	@Column(name="date")
	@JsonProperty
	private Date date;

	@ElementCollection (fetch=FetchType.EAGER)
	@CollectionTable(name="rates" , joinColumns=@JoinColumn(name="rating_id"))
	@MapKeyColumn(name="name")
	@Column(name="value")
	private Map<String, Float> rates;
	
	public Rating() {
		super();
	}

	public Rating(String device, Date date, Map<String, Float> rates) {
		super();
		this.device = device;
		this.date = date;
		this.rates = rates;
	}
	
	public Rating(int id, String device, Date date, Map<String, Float> rates) {
		super();
		this.id = id;
		this.device = device;
		this.date = date;
		this.rates = rates;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Map<String, Float> getRates() {
		return rates;
	}

	public void setRates(Map<String, Float> rates) {
		this.rates = rates;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((device == null) ? 0 : device.hashCode());
		result = prime * result + id;
		result = prime * result + ((rates == null) ? 0 : rates.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rating other = (Rating) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (device == null) {
			if (other.device != null)
				return false;
		} else if (!device.equals(other.device))
			return false;
		if (id != other.id)
			return false;
		if (rates == null) {
			if (other.rates != null)
				return false;
		} else if (!rates.equals(other.rates))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Rating [rates=" + rates + "]";
	}

	public int compareTo(Rating another) {
		return this.date.compareTo(another.getDate());
	}
	
	
	
}
