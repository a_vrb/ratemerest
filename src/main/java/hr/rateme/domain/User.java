package hr.rateme.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.authority.AuthorityUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="users")
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7628213687835764971L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	@JsonProperty
	private long id;
	
	@Column(name="email")
	@JsonProperty
	private String email; // username
	
	@Column(name="password")
	@JsonProperty
	private String password; // should be hashed, but doesn't matter in our example
	
	@Column(name="firstname")
	@JsonProperty
	private String firstname; // full name
	
	@Column(name="lastname")
	@JsonProperty
	private String lastname;

	@OneToMany
	@JoinTable(name = "users_devices", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "device_id") })
	private List<Device> devices;
	
	@OneToMany
	@JoinTable(name="users_places", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "place_id") })
	private List<Place> places;
	/*	private String[] roles;
	
	*/
	
	public User(){}
	
	public User(long id, String email) {
		super();
		this.id = id;
		this.email = email;
	}
	
	public User(long id, String email, String password) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
	}
	
	

//	public User(long id, String email, String password, String[] roles) {
//		super();
//		this.id = id;
//		this.email = email;
//		this.password = password;
//		this.roles = roles;
//	}
	
//	public User(String email, String password, String[] roles) {
//		super();
//		this.email = email;
//		this.password = password;
//		this.roles = roles;
//	}

	public User(long id, String email, String password, String firstname, String lastname) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	

	public User(long id, String email, String firstname, String lastname) {
	super();
	this.id = id;
	this.email = email;
	this.firstname = firstname;
	this.lastname = lastname;
}



	public User(String email, String password, String firstname, String lastname) {
		super();
		this.email = email;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public User(String email, String password, String firstname, String lastname, List<Device> devices, List<Place> places) {
		super();
		this.email = email;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.devices = devices;
		this.places = places;
	}

//	public User(String email, String password, String firstname, String lastname, List<Device> devices, String[] roles) {
//		super();
//		this.email = email;
//		this.password = password;
//		this.firstname = firstname;
//		this.lastname = lastname;
//		this.devices = devices;
//		this.roles = roles;
//	}

//	public User(long id, String email, String password, String firstname, String lastname, List<Device> devices, List<Place> places, String[] roles) {
//		super();
//		this.id = id;
//		this.email = email;
//		this.password = password;
//		this.firstname = firstname;
//		this.lastname = lastname;
//		this.devices = devices;
//		this.places = places;
//		this.roles = roles;
//	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public List<Place> getPlaces() {
		return places;
	}

	public void setPlaces(List<Place> places) {
		this.places = places;
	}

//	public String[] getRoles() {
//	return roles;
//}
//
//public void setRoles(String[] roles) {
//	this.roles = roles;
//}
//
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@JsonIgnore
	public org.springframework.security.core.userdetails.User getSpringUser(){
		return new org.springframework.security.core.userdetails.User(this.email, this.password, AuthorityUtils.createAuthorityList("ROLE_USER"));
	}
	


}
