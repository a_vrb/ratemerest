package hr.rateme.rest.controller;

import hr.rateme.domain.Device;
import hr.rateme.domain.User;
import hr.rateme.rest.security.TokenAuthenticationService;
import hr.rateme.rest.security.UserAuthentication;
import hr.rateme.rest.services.DevicesRateMeService;
import hr.rateme.rest.services.UsersRateMeService;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class AuthController {
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private TokenAuthenticationService authenticationService;
	@Autowired
	private AuthenticationManager manager;
	
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private UsersRateMeService usersService;
	
	@Autowired
	private DevicesRateMeService devicesService;
	
	@PostConstruct
	public void init() {
		System.out.println(" *** AuthController.init with: " + applicationContext);
	}
	
	@RequestMapping(value = "rateme/users/{email:.+}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public User getUser(@PathVariable("email") String email, HttpServletRequest request, HttpServletResponse response) {
		return usersService.getUserByUsername(email);

	}
	
//	@RequestMapping(value = "rateme/users/{id}", method = RequestMethod.GET)
//	@ResponseStatus(HttpStatus.OK)
//	@ResponseBody
//	public User getUserById(@PathVariable("id") long id, HttpServletRequest request, HttpServletResponse response) {
//		return usersService.getUser(id);
//
//	}
	
	@RequestMapping(value = "rateme/devices/{uuid}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Device getDevice(@PathVariable("uuid") String uuid, HttpServletRequest request,HttpServletResponse response) {
		return devicesService.getDeviceByUUID(uuid);

	}
	
	@RequestMapping(value = "rateme/devices/{uuid}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void postDeviceData(@RequestBody Device d, HttpServletRequest request,HttpServletResponse response) {
		 devicesService.updateDevice(d);
	}
	
	@RequestMapping(value = "rateme/register/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void registerUser(@RequestBody User user, HttpServletRequest request,HttpServletResponse response) {
		usersService.registerUser(user);
	}
	
	@RequestMapping(value = "rateme/register/device", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void registerDevice(@RequestBody Device d, HttpServletRequest request,HttpServletResponse response) {
		Device dev = devicesService.getDeviceByUUID(d.getUuid());
	    UUID uuid = UUID.fromString(d.getUuid());
	   
		if(dev !=null || !uuid.toString().equals(d.getUuid())){
		response.setStatus(HttpStatus.BAD_REQUEST.value());//setHeader("Location", request.getRequestURL().append("/").append(dev.getUuid()).toString());
		return;
		}
		devicesService.registerDevice(d);
		response.setHeader("Location", request.getRequestURL().append("/").append(d.getUuid()).toString());
		response.setStatus(HttpStatus.ACCEPTED.value());
	}
	
	@RequestMapping(value = "/rateme/auth/login/user",method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE } )
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	public User login(HttpServletRequest request,HttpServletResponse response) {
		String username = request.getHeader("X-Username");
		String password = request.getHeader("X-Password");
		UserAuthentication u = new UserAuthentication(new org.springframework.security.core.userdetails.User(username, password, AuthorityUtils.createAuthorityList("ROLE_USER")));
		u.setAuthenticated(manager.authenticate(new UsernamePasswordAuthenticationToken(u.getPrincipal(), u.getCredentials())).isAuthenticated());
		
		if(u.isAuthenticated()){
        	authenticationService.addAuthentication(response, u); 
        	User user = usersService.getUserByUsername(username);
        	user.setPassword("");
        	return user;
		}
		return null;
	}

//	@RequestMapping(value = "/logout", method = RequestMethod.GET)
//	public String logout() {
//		System.out.println(" *** AuthController.logout");
//		return "Logout invalidates token on server-side. It must come as a POST request with valid X-Auth-Token, URL is configured for MyAuthenticationFilter.";
//	}
}
