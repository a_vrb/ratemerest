package hr.rateme.rest.controller;
import hr.rateme.domain.Comment;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Rating;
import hr.rateme.rest.services.RateMeService;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;


@PropertySource({ "classpath:app.properties" })
@Controller
@RequestMapping( value = "rateme", produces = { MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8" } )
public class RestController {
	@Autowired
	private Environment env;
	
	@Autowired
	private RateMeService service;
	
	@RequestMapping(value = "places/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Place getPlace(@PathVariable("id") int id, HttpServletRequest request,HttpServletResponse response) {
		return service.getPlace(id);

	}
	@RequestMapping(value = "places", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Place> getPlaces(HttpServletRequest request,HttpServletResponse response){
		return service.getPlaces();
	}
	
	@RequestMapping(value = "places/location", method = RequestMethod.GET, params = {"latitude", "longitude", "radius"})
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<GeoLocation> getPlacesByLocation(@RequestParam("latitude") double latitude,@RequestParam("longitude") double longitude,@RequestParam("radius") double radius,HttpServletRequest request,HttpServletResponse response){
		return service.findPlacesByLocation(latitude,longitude, radius/1000);
	}
	
	@RequestMapping(value="places/{id}/comment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public void saveComment(@RequestBody Comment c,@PathVariable("id") int id, HttpServletRequest request, HttpServletResponse response){
		service.saveComment(c,id);
		response.setHeader("Location", request.getRequestURL().append("/").append(c.getId()).toString());
	}
	
	
	@RequestMapping(value="places/{id}/promotion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public void savePromotion(@RequestBody Promotion p,@PathVariable("id") int id, HttpServletRequest request, HttpServletResponse response){
		service.savePromotion(p,id);
        response.setHeader("Location", request.getRequestURL().append("/").append(p.getId()).toString());
	}
	
	@RequestMapping(value="places/{id}/rating", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public void saveRating(@RequestBody Rating r,@PathVariable("id") int id, HttpServletRequest request, HttpServletResponse response){
		service.saveRating(r,id);
		 response.setHeader("Location", request.getRequestURL().append("/").append(r.getId()).toString());
	}
	
	@RequestMapping(value="places/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void updatePlace(@RequestBody Place p, HttpServletRequest request, HttpServletResponse response){
		service.updatePlace(p);
		 response.setHeader("Location", request.getRequestURL().append("/").append(p.getId()).toString());
	}
	
	@RequestMapping(value="places", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Place savePlace(@RequestBody Place p, HttpServletRequest request, HttpServletResponse response){
		service.savePlace(p);
		response.setHeader("Location", request.getRequestURL().append("/").append(p.getId()).toString());
		return p;
	}
	
	@RequestMapping(value="comments/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void updateComment(@RequestBody Comment c, HttpServletRequest request, HttpServletResponse response){
		service.updateComment(c);
		 response.setHeader("Location", request.getRequestURL().append("/").append(c.getId()).toString());
	}
	
	@RequestMapping(value="places/{id}/picture", method=RequestMethod.POST)
	public @ResponseBody String uploadPicture(@PathVariable("id") int id, @RequestParam("file") MultipartFile file){
		
		if(file.isEmpty()){
			return "Error empty file";
		}
		try {
			file.transferTo(new File(System.getenv(env.getProperty("app.pictures.data_dir"))+"pictures/"+file.getOriginalFilename()));
			Place p = service.getPlace(id);
			p.setPictureUrl(env.getProperty("app.pictures.url")+file.getOriginalFilename());
			service.updatePlace(p);
			
		} catch (IllegalStateException | IOException e) {
			return e.getMessage();
		}
		
		return "";
	}
}
