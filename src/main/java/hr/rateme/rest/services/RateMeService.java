package hr.rateme.rest.services;

import hr.rateme.domain.Comment;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Rating;

import java.util.List;

public interface RateMeService {
	
	public List<Place> getPlaces();
	public Place getPlace(int id);
	List<GeoLocation> findPlacesByLocation(double latitude, double longitude, double radius);
	public void saveComment(Comment c, int place_id);
	public void updateComment(Comment c);
	public void savePromotion(Promotion p, int place_id);
	public void saveRating(Rating r, int place_id);
	public void updatePlace(Place p);
	public void savePlace(Place p);
}
