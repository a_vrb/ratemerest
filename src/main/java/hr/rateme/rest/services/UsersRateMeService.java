package hr.rateme.rest.services;

import hr.rateme.domain.User;

public interface UsersRateMeService {
	public User getUserByUsername(String email);
	public User getUser(long id);
	public void registerUser(User user);
}
