package hr.rateme.rest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.rateme.domain.Device;
import hr.rateme.rest.dao.DevicesDao;

@Service
@Transactional
public class DevicesRateMeServiceImpl implements DevicesRateMeService {
	
	@Autowired
	private DevicesDao devicesDao; 
	
	
	@Override
	public Device getDeviceByUUID(String uuid) {
		return devicesDao.getDeviceByUUID(uuid);
	}

	@Override
	public void registerDevice(Device device) {
		devicesDao.registerDevice(device);
	}

	@Override
	public void updateDevice(Device device) {
		devicesDao.updateDevice(device);
	}
}
