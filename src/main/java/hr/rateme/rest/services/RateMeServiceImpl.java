package hr.rateme.rest.services;

import hr.rateme.domain.Comment;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Rating;
import hr.rateme.rest.dao.PlacesDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RateMeServiceImpl implements RateMeService {

	@Autowired
	private PlacesDao placesDao;
	
	public Place getPlace(int id) {
		return placesDao.findPlaceById(id);
	}

	public List<Place> getPlaces() {
		return placesDao.findAllPlaces();
	}
	
	public void saveComment(Comment c, int place_id) {
		placesDao.saveComment(c, place_id);
		
	}
	
	public void savePromotion(Promotion p, int place_id) {
		placesDao.savePromotion(p, place_id);
		
	}

	public void saveRating(Rating r, int place_id) {
		placesDao.saveRating(r, place_id);
		
	}

	public void updatePlace(Place p) {
		placesDao.updatePlace(p);
		
	}
	
	public void savePlace(Place p) {
		placesDao.savePlace(p);
		
	}

	public void updateComment(Comment c) {
		placesDao.updateComment(c);
		
	}

	@Override
	public List<GeoLocation> findPlacesByLocation(double latitude,
			double longitude, double radius) {
		return placesDao.findPlacesByLocation(latitude, longitude, radius);
	}

	
	
}
