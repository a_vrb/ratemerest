package hr.rateme.rest.services;

import hr.rateme.domain.Device;

public interface DevicesRateMeService {

	public Device getDeviceByUUID(String uuid);
	public void registerDevice(Device device);
	public void updateDevice(Device device);
}
