package hr.rateme.rest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.rateme.domain.User;
import hr.rateme.rest.dao.UsersDao;

@Service
@Transactional
public class UsersRateMeServiceImpl implements UsersRateMeService {

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public User getUserByUsername(String email) {
		return usersDao.getUserByUsername(email);
	}
	
	@Override
	public User getUser(long id){
		return usersDao.getUser(id);
	}

	@Override
	public void registerUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		usersDao.registerUser(user);
	}

}
