package hr.rateme.rest.security;


import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UserAuthentication extends UsernamePasswordAuthenticationToken {
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 7254614637994337083L;
	private final User user;
    private boolean authenticated = true;
 
    public UserAuthentication(User user) {
        super(user.getUsername(), user.getPassword());
    	this.user = user;
        
    }
    
 
    @Override
    public String getName() {
        return user.getUsername();
    }
 
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return user.getAuthorities();
    }
 
    @Override
    public Object getCredentials() {
        return user.getPassword();
    }
 
    @Override
    public User getDetails() {
        return user;
    }
 
    @Override
    public Object getPrincipal() {
        return user.getUsername();
    }
 
    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }
 
    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}