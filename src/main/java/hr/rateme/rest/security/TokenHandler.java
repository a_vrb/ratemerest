package hr.rateme.rest.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Calendar;

import org.springframework.security.core.userdetails.User;

public final class TokenHandler {
	 
    private final String secret;
    private final UserAuthService userService;
 
    public TokenHandler(String secret, UserAuthService userService) {
        this.secret = secret;
        this.userService = userService;
    }
 
    public User parseUserFromToken(String token) {
        String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return userService.loadUserByUsername(username);
    }
 
    public String createTokenForUser(User user) {
    	Calendar cal = Calendar.getInstance();
    	cal.setTimeInMillis(Calendar.getInstance().getTimeInMillis()+1200000);
        return Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(Calendar.getInstance().getTime())
                .setExpiration(cal.getTime())
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
}