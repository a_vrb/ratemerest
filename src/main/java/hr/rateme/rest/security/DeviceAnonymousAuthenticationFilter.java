package hr.rateme.rest.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;

public class DeviceAnonymousAuthenticationFilter extends AnonymousAuthenticationFilter {
	private static final String X_HEADER_DEVICE_ID = "X-AUTH-DEVICE";
	
	private String principalParameter = X_HEADER_DEVICE_ID;
	
	private AuthenticationManager authenticationManager;
	
	public DeviceAnonymousAuthenticationFilter(AuthenticationManager authenticationManager) {
		super("key");
		this.authenticationManager = authenticationManager;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		
		if(request.getHeader(principalParameter)!=null){
		AnonymousAuthenticationToken aat = (AnonymousAuthenticationToken)createAuthentication(request);
		Authentication auth = authenticationManager.authenticate(aat);
		SecurityContextHolder.getContext().setAuthentication(auth);
		}
        
        chain.doFilter(req, res);
        SecurityContextHolder.getContext().setAuthentication(null);
	}

	@Override
	protected Authentication createAuthentication(HttpServletRequest request) {
		String principal = request.getHeader(principalParameter);
		return new AnonymousAuthenticationToken(principal, principal, AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
	}

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
	
	
}
