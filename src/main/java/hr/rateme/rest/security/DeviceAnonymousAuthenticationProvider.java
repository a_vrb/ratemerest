package hr.rateme.rest.security;

import hr.rateme.rest.services.DevicesRateMeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class DeviceAnonymousAuthenticationProvider extends AnonymousAuthenticationProvider {
	
	@Autowired
	DevicesRateMeService devicesService;
	
	public DeviceAnonymousAuthenticationProvider() {
		super("anonimna_kozu_ima_meku_kao_svila");
		
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (!supports(authentication.getClass())) {
            return null;
        }
		
        if (devicesService.getDeviceByUUID(authentication.getPrincipal().toString().trim())==null) {
            throw new BadCredentialsException(messages.getMessage("AnonymousAuthenticationProvider.incorrectKey",
                    "The presented AnonymousAuthenticationToken does not contain the expected key"));
        }
        
        authentication.setAuthenticated(true);
        return authentication;
	}

}
