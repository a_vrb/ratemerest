package hr.rateme.rest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("hr.rateme.rest.services")
public class ServiceConfig {
   
     

}
