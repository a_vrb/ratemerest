package hr.rateme.rest.config;

import hr.rateme.rest.security.DeviceAnonymousAuthenticationFilter;
import hr.rateme.rest.security.DeviceAnonymousAuthenticationProvider;
import hr.rateme.rest.security.RestAuthenticationEntryPoint;
import hr.rateme.rest.security.StatelessAuthenticationFilter;
import hr.rateme.rest.security.TokenAuthenticationService;
import hr.rateme.rest.security.UserAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebMvcSecurity
@ComponentScan({ "hr.rateme.rest" })
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserAuthService userService;
    private final TokenAuthenticationService tokenAuthenticationService;
    
    @Autowired
    private DeviceAnonymousAuthenticationProvider anonymousAuthenticationProvider;

    public SecurityConfig() {
        super(true);
        this.userService = new UserAuthService();
        tokenAuthenticationService = new TokenAuthenticationService("r473m353cr37", userService);
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                	.authenticationEntryPoint(authEntryPoint())
                .and()
                .anonymous()
                	.authenticationFilter(new DeviceAnonymousAuthenticationFilter(authenticationManagerBean()))
                	.authenticationProvider(anonymousAuthenticationProvider)
                .and()
                .servletApi().and()
                .headers().cacheControl().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
               // .httpBasic().and()
                	.authorizeRequests()
 
                // Allow anonymous resource requests
                	.antMatchers("/").permitAll()
 
                // Allow anonymous logins
                	.antMatchers("/rateme/register/user").hasRole("ANONYMOUS")
                	.antMatchers("/rateme/devices/**").hasRole("ANONYMOUS")
                	.antMatchers("/rateme/auth/**").hasRole("ANONYMOUS")
                	.antMatchers(HttpMethod.POST,"/rateme/places").hasRole("ANONYMOUS")
                	.antMatchers(HttpMethod.PUT, "/rateme/places/{id}").authenticated()
                	.antMatchers("/rateme/places/{id}/promotion").authenticated()
                	.antMatchers("/rateme/places/{id}/picture").authenticated()
                	.antMatchers("/rateme/places/**").hasRole("ANONYMOUS")
 
                // All other request need to be authenticated
                	.anyRequest().authenticated().and()
 
                // Custom Token based authentication based on the header previously given to the client
                	.addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class);
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
      web
        .ignoring()
           .antMatchers("/rateme/register/device")
           .antMatchers("/pictures/**");
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }
 
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
 
    @Bean
    @Override
    public UserAuthService userDetailsService() {
        return userService;
    }
 
    @Bean
    public TokenAuthenticationService tokenAuthenticationService() {
        return tokenAuthenticationService;
    }
    
	@Bean
	public AuthenticationEntryPoint authEntryPoint() {
		return new RestAuthenticationEntryPoint();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
}
