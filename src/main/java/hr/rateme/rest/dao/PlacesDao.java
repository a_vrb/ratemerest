package hr.rateme.rest.dao;

import hr.rateme.domain.Comment;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Rating;

import java.util.List;

public interface PlacesDao {

	List<Place> findAllPlaces();
	Place findPlaceById(int id);
	List<GeoLocation> findPlacesByLocation(double latitude, double longitude, double radius);
	void saveComment(Comment c, int place_id);
	void savePromotion(Promotion p, int place_id);
	void saveRating(Rating r, int place_id);
	
	void updateComment(Comment c);
	void updatePlace(Place p);
	void savePlace(Place p);
}
