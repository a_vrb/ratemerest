package hr.rateme.rest.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hr.rateme.domain.Device;

@Repository
public class DevicesDaoImpl implements DevicesDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public Device getDeviceByUUID(String uuid) {
		return (Device) getCurrentSession().get(Device.class, uuid);
	}

	@Override
	public void registerDevice(Device device) {
		getCurrentSession().saveOrUpdate(device);

	}

	@Override
	public void updateDevice(Device device) {
		getCurrentSession().update(device);

	}
}
