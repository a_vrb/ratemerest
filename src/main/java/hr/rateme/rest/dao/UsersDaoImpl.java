package hr.rateme.rest.dao;

import hr.rateme.domain.Place;
import hr.rateme.domain.User;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UsersDaoImpl implements UsersDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public User getUserByUsername(String email) {
//		Query q = getCurrentSession().createQuery("select new User(u.id, u.firstname, u.lastname, u.email) from User as u where email = :email");
//		q.setParameter("email", email);
//		return (User) q.uniqueResult();
		User u = (User) getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult();
		Hibernate.initialize(u.getDevices());
		Hibernate.initialize(u.getPlaces());
		for(Place p : u.getPlaces()){
			Hibernate.initialize(p.getRatings());
			Hibernate.initialize(p.getComments());
			Hibernate.initialize(p.getPromotions());
		}
		return u;
	}
	
	@Override
	public User getUser(long id){
		User user = (User) getCurrentSession().get(User.class, id);
		Hibernate.initialize(user.getDevices());
		Hibernate.initialize(user.getPlaces());
		for(Place p : user.getPlaces()){
			Hibernate.initialize(p.getRatings());
			Hibernate.initialize(p.getPromotions());
			Hibernate.initialize(p.getComments());
		}
		return user;
	}

	@Override
	public void registerUser(User user) {
		getCurrentSession().save(user);

	}

}
