package hr.rateme.rest.dao;

import hr.rateme.domain.Device;

public interface DevicesDao {
	public Device getDeviceByUUID(String uuid);
	public void registerDevice(Device device);
	public void updateDevice(Device device);
}
