package hr.rateme.rest.dao;

import hr.rateme.domain.User;

public interface UsersDao {
	public User getUserByUsername(String email);
	public User getUser(long id);
	public void registerUser(User user);
}
