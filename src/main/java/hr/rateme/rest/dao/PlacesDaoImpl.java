package hr.rateme.rest.dao;

import hr.rateme.domain.Comment;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Rating;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PlacesDaoImpl implements PlacesDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public Place findPlaceById(int id) {
		Place p = (Place) getCurrentSession().get(Place.class, id);
		Hibernate.initialize(p.getRatings());
		Hibernate.initialize(p.getComments());
		Hibernate.initialize(p.getPromotions());
		return p;
	}

	@SuppressWarnings("unchecked")
	public List<Place> findAllPlaces() {
		/*	Session s = getCurrentSession();
		List<Place> places = s.createQuery("from Place").list();
		for(Place p: places){
			Hibernate.initialize(p.getRatings());
			Hibernate.initialize(p.getComments());
			Hibernate.initialize(p.getPromotions());
			
		}
		return places;*/
		//getCurrentSession().createQuery("from Place").list();
		return getCurrentSession().createQuery("select new Place(p.id, p.name, p.address) from Place as p").list();
		//return getCurrentSession().createCriteria(Place.class).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GeoLocation> findPlacesByLocation(double latitude,
			double longitude, double radius) {
		List<GeoLocation> result = (List<GeoLocation>)getCurrentSession().createSQLQuery("CALL findPlacesByLocation(:lat, :long, :distance)")
				.addEntity(GeoLocation.class)
				.setParameter("lat", latitude)
				.setParameter("long", longitude)
				.setParameter("distance", radius).list();
		for(GeoLocation g : result){
			Hibernate.initialize(g.getPlace().getRatings());
			Hibernate.initialize(g.getPlace().getComments());
			Hibernate.initialize(g.getPlace().getPromotions());
		}
	
		return result;
	}
	
	public void saveComment(Comment c, int places_id) {
		Place p = (Place)getCurrentSession().load(Place.class, places_id);
		p.getComments().add(c);
		getCurrentSession().update(p);
		/*int id = (Integer)getCurrentSession().save(c);
		SQLQuery query = getCurrentSession().createSQLQuery("UPDATE comments SET place_id = :place_id WHERE id = :id");
		query.setParameter("place_id",places_id);
		query.setParameter("id",id);
		query.executeUpdate();*/
		
	}

	public void updateComment(Comment c){
		getCurrentSession().update(c);
		
	}
	public void savePromotion(Promotion p, int places_id) {
		Place pl = (Place)getCurrentSession().load(Place.class, places_id);
		pl.getPromotions().add(p);
		getCurrentSession().update(pl);
//		int id = (Integer)getCurrentSession().save(p);
//		SQLQuery query = getCurrentSession().createSQLQuery("UPDATE promotions SET place_id = :place_id WHERE id = :id");
//		query.setParameter("place_id",places_id);
//		query.setParameter("id",id);
//		query.executeUpdate();
	}

	public void saveRating(Rating r, int places_id) {
		Place p = (Place)getCurrentSession().load(Place.class, places_id);
		p.getRatings().add(r);
		getCurrentSession().update(p);
//		int id = (Integer) getCurrentSession().save(r);
//		SQLQuery query = getCurrentSession().createSQLQuery("UPDATE ratings SET place_id = :place_id WHERE id = :id");
//		query.setParameter("place_id",places_id);
//		query.setParameter("id",id);
//		query.executeUpdate();
	}

	public void updatePlace(Place p) {
		getCurrentSession().update(p);
	}

	public void savePlace(Place p){
		getCurrentSession().save(p);
	}


	
	

	
}
